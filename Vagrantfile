# -*- mode: ruby -*-
# vi: set ft=ruby :
require "./vagrant_vars.rb"
include Variables

####################################### check errors #######################################
if VM_COUNT <= 0
  abort "Incorrect number of virtual machines. Variable VM_COUNT must be greater than 0."
end
if VM_HOSTS.size == 0
  abort "Variable VM_HOSTS is empty."
end
VM_HOSTS.each do |check|
  if check.respond_to?(:empty?) && check.empty? 
    abort "Some elements of VM_HOSTS is empty."
  end
end
if NEED_ANSIBLE == true
  if ANSIBLE_HOSTNAME == ""
    ANSIBLE_HOSTNAME = "ansible" + time.year + time.month + time.day + "-" + time.hour + time.min + time.sec
  end
  if VM_COUNT > VM_HOSTS.size + 1
    abort "Incorrect number of virtual machines. Variable VM_COUNT is greater than number of hosts in VM_HOSTS."
  end
  if ANSIBLE_TYPE != "ansible" && ANSIBLE_TYPE != "ansible_local"
    abort "Incorrect type of ansible provision. It must to be 'ansible' or 'ansible_local'"
  end
else
  if VM_COUNT > VM_HOSTS.size
    abort "Incorrect number of virtual machines. Variable VM_COUNT is greater than number of hosts in VM_HOSTS."
  end
end

####################### define standard virtual machines parameters #######################
def standard_vm_create(config, hostname, ip_address, script)
  config.vm.define hostname do |host|
    host.vm.provider "virtualbox" do |vb|
      vb.name = hostname
      vb.cpus = "#{VM_CPU_CORES}"
      vb.memory = "#{VM_MEMORY}"
    end
    host.vm.hostname = hostname
    host.vm.network "#{NETWORK_TYPE}_network", bridge: ETHERNET_CARD, ip: ip_address
    if script != ""
      config.vm.provision "shell", path: script
    end
  end
end

################### create virtual machines with additional parameters ####################
Vagrant.configure("2") do |config|
  config.vm.box = "#{OS_BOX}"
  config.vm.box_check_update = false
  config.vm.boot_timeout = 900
  (1..VM_COUNT).each do |count|
    if (count == VM_COUNT) && (NEED_ANSIBLE == true)
      standard_vm_create(config, ANSIBLE_HOSTNAME, "#{IP_RANGE}.10", "vagrant_script.sh")
    else
      standard_vm_create(config, VM_HOSTS[count-1][0], "#{IP_RANGE}.#{10+count}", "vagrant_script.sh")
      if VM_HOSTS[count-1].size > 1
        (1..VM_HOSTS[count-1].size-1).each do |ports_count|
          config.vm.define VM_HOSTS[count-1][0] do |host|
            ports = VM_HOSTS[count-1][ports_count].split(":")
            host.vm.network  "forwarded_port", guest: ports[0], host: ports[1]
          end
        end
      end
    end
  end 
end
