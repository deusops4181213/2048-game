
# 2048-game

## Ansible pipeline

## Terraform Helm YandexCloud pipeline


### CI Variables

* ANSIBLE_USER - ansible user 
* SSH_KEY - File type - ansible user ssh priv key
* HOSTS - File type - *ansible/inventories/hosts*
* DOCKER_DIR - docker build dir  
* APPLICATION_NAME - app name
* APPLICATION_REPO - app repo
* BACKEND_CONF - File type - *terraform/backend.hcl*. Backend configuration
* TERRAFORM_DIR - terraform dir
* HELM_CHART_DIR - chart dir
* TF_VAR_yandex_zone - prefered zone 
* TF_VAR_yandex_cloud_id - your cloid id
* TF_VAR_yandex_folder_id - your folder id
* TF_VAR_yandex_yandex_sa_key_file - sa authorized key (json)
* TF_VAR_nodes_metadata - File type - *terraform/meta.yml*. user:ssh key pair for ssh cloud VM
* TF_VAR_state_bucket_name - bucket name