variable "yandex_sa_key_file" {
  description = "service account key file"
  type = string
  default = "../key.json"
}
variable "yandex_cloud_id" {
  description = "cloud id"
  type = string
  default = "b1gdnb5qq6j2b0ave8vp"
}
variable "yandex_folder_id" {
  description = "folder id"
  type = string
  default = "b1g4v5jtjk1cm2ab0os0"
}
variable "yandex_zone" {
  description = "availibility zones: ru-central1-a, ru-central1-b, ru-central1-c (will be disabled)"
  type = string
  default = "ru-central1-a"
}
variable "state_bucket_name" {
  description = "bucket names are unique throughout Object Storage"
  type = string
  default = "tfstate-b1g4v5jtjk1cm2ab0os0"
}