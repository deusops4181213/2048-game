terraform {
  required_version = ">= 0.13"
  required_providers {
    yandex   = {
      source = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  service_account_key_file = file(var.yandex_sa_key_file)
  cloud_id                 = var.yandex_cloud_id
  folder_id                = var.yandex_folder_id
  zone                     = var.yandex_zone
}

resource "yandex_iam_service_account" "sa" {
  folder_id = var.yandex_folder_id
  name      = "remote-state-sa"
}

resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = var.yandex_folder_id
  role             = "editor"
  member           = "serviceAccount:${yandex_iam_service_account.sa.id}"
}

resource "yandex_iam_service_account_static_access_key" "sa_static_key" {
  service_account_id = yandex_iam_service_account.sa.id
}

resource "yandex_storage_bucket" "state_bucket" {
  force_destroy = true
  bucket        = var.state_bucket_name
  access_key    = yandex_iam_service_account_static_access_key.sa_static_key.access_key
  secret_key    = yandex_iam_service_account_static_access_key.sa_static_key.secret_key
}