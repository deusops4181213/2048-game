resource "yandex_vpc_security_group" "secgroup_k8s" {
  name        = "secgroup-k8s"
  network_id  = yandex_vpc_network.network_a.id
# allow loadbalancer healthchecks
  ingress {
    protocol          = "TCP"
    predefined_target = "loadbalancer_healthchecks"
    from_port         = var.application_port
    to_port           = var.application_port
  }
# allow internal traffic
  ingress {
    description       = "Allow traffic for Master-Node and Node-Node"
    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    description    = "Allow traffic for Pod-Pod and Service-Service"
    protocol       = "ANY"
    v4_cidr_blocks = concat(yandex_vpc_subnet.subnet_a.v4_cidr_blocks)
    from_port      = 0
    to_port        = 65535
    }
# allow whitelist traffic
  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = var.whitelist_ip
    from_port      = var.application_port
    to_port        = var.application_port
  }
# allow management connections
  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = [var.management_ip]
    from_port      = 443
    to_port        = 443
  }
  ingress {
    protocol       = "TCP"
    from_port      = var.management_port
    to_port        = var.management_port
    v4_cidr_blocks = [var.management_ip]
  }
# allow all outgoing traffic
  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}
