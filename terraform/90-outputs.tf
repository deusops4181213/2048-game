output "k8s_external_address" {
  value = yandex_kubernetes_cluster.k8s_cluster.master.0.external_v4_endpoint
}

output "game_address" {
  value = format("%s%s", "http://", data.kubernetes_service.ingress_nginx.status.0.load_balancer.0.ingress.0.ip)
}
