resource "helm_release" "ingress_nginx" {
  name       = var.ingress_name
  repository = var.ingress_chart_repo
  chart      = var.ingress_chart_name
  namespace   = var.namespace_ingress
  wait       = true
  values = [
    "${file(var.ingress_values_path)}"
  ]
  depends_on = [
    yandex_kubernetes_node_group.nodes,
    kubernetes_namespace.namespace_ingress
  ]
}

data "kubernetes_service" "ingress_nginx" {
  metadata {
    name      = "ingress-nginx-controller"
    namespace = var.namespace_ingress
  }
  depends_on = [
    helm_release.ingress_nginx
  ]
}
