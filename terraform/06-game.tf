resource "helm_release" "game" {
  name       = var.game_chart_name
  chart      = var.game_chart_path
  wait       = true
  values = [
    "${file(var.game_values_path)}"
  ]
  depends_on = [
    helm_release.ingress_nginx
  ]
}

resource "kubernetes_ingress_v1" "ingress" {
  metadata {
    name        = var.ingress_name
    namespace   = var.namespace_game
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }
  spec {
    rule {
      http {
        path {
          path = "/"
          backend {
            service {
              name = var.game_service_name
              port {
                number = var.application_port
              }
            }
          }
        }
      }
    }
  }
  depends_on = [
    helm_release.ingress_nginx
  ]
}
