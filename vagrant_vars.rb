# variables for vagrant

module Variables

  # number of virtual machines include ansible host
    VM_COUNT = 4
  # set ethernet card name
    #ETHERNET_CARD = "Realtek Gaming 2.5GbE Family Controller"
    ETHERNET_CARD = "Intel(R) Wi-Fi 6E AX210 160MHz"
  # number of cpu cores for each virtual machine
    VM_CPU_CORES = 2
  # amount of memory for each virtual machine
    VM_MEMORY = 2048
  # network type for project ("private" or "public")
    NETWORK_TYPE = "private"
  # ip subnet: first three octets with delimiters
    IP_RANGE = "192.168.100"
  # set OS box
    OS_BOX = "debian/bullseye64"
  # "true" if you need ansible in this project
    NEED_ANSIBLE = false
  # type of ansible provision: "ansible" or "ansible_local"
    ANSIBLE_TYPE = "ansible"
  # ansible host name
    ANSIBLE_HOSTNAME = "ansible01"
  # list of vm names and forwarding ports except ansible. must to be specified as:
  # [["vm1-name", "guest-port1:host-port1", .., "guest-portN:host-portN"],
  # ..,
  # ["vmN-name", "guest-port1:host-port1", .., "guest-portN:host-portN"]]
    VM_HOSTS = [
      ["game01"],
      ["game02"],
      ["game03"],
      ["proxy01", "80:80"]
    ]
  
  end
  